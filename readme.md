﻿# Assignment 6
Codeigniter Login System and CRUD with JQuery 

## Features
- Login System
- Select table from database
- Insert table from database
- Update table from database
- Delete table from database
- Search 

## Requirements
 -  [Visual Studio Code](https://code.visualstudio.com/) 
 - [AppServ](https://www.appserv.org/th/) : Apache + PHP + MySQL
 - [CodeIgniter](https://codeigniter.com/download) : 3.1.5 or greater
 -  [JQuery](https://jquery.com/) : 3.3.1
 -  [Bootstrap](https://getbootstrap.com/) : 3.3.7

## Installation
 - Install Visual Studio Code
 - Install AppServ 
 - Unzip file CodeIgniter and copy-paste all the files to directory in
	`` C:\AppServ\www ``
 - Download zip file bootstrap/JQuery and copy-paste all the files to directory in 
	``C:\AppServ\www\foldername\assets``
or put this link in your views
	
	 - ``<link  rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">``
	 - ``<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>``
	 - ``<script  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>``

## Config

 - autoload 
	 - `$autoload['libraries'] = array('database', 'session');`
	 - `$autoload['helper'] = array('url');`
 - config
	 - `$config['base_url'] = 'http://localhost/foldername/';`

	 

## How to run 
open browser put this URL : `http://localhost/foldername`

