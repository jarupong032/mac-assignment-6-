<?php 
class User extends CI_Controller
{


	public function __construct(){
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('Model');
	}

	// book_model = Model
	// books = ex_users (databas)
	// book_view = show_data
	// book_id = user_id

	public function index(){
        $this->load->view("login");
		$this->load->view('header');
		$this->Model->isLoggedIn();
        $this->load->view('show_data');
		
    }

	public function logout(){
        $this->session->sess_destroy();
        redirect('/' ,'refresh');
        exit;
	}
	
	public function login(){
        $username =  $this->input->post('username');
        $password =  $this->input->post('password');
        
        if($this->Model->login($username, $password)){
            }
        else{
            echo'something went wrong';
        }
    }
	
	public function get_all()
    {
		$this->load->view('header');
		$data['ex_users']=$this->Model->get_all();
		$this->load->view('show_data',$data);
	}
	
	public function add_user(){
			$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password'),
					'user_firstname' => $this->input->post('user_firstname'),
					'user_lastname' => $this->input->post('user_lastname'),
					'user_email' => $this->input->post('user_email'),
					'user_phone' => $this->input->post('user_phone'),
				);
			$insert = $this->Model->add_user($data);
			echo json_encode(array("status" => TRUE));
	}

	public function edit_user($user_id){
			$data = $this->Model->get_id($user_id);
			echo json_encode($data);
	}
	
	public function user_update(){
			$data = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'user_firstname' => $this->input->post('user_firstname'),
				'user_lastname' => $this->input->post('user_lastname'),
				'user_email' => $this->input->post('user_email'),
				'user_phone' => $this->input->post('user_phone'),
				);
			$this->Model->user_update(array('user_id' => $this->input->post('user_id')), $data);
			echo json_encode(array("status" => TRUE));
	}

	public function delete_user($id){
			$this->Model->delete_id($id);
			echo json_encode(array("status" => TRUE));
	}

}
?>