<?php 

if (!defined('BASEPATH'))
        exit('No direct script access allowed');
class Model extends CI_Model{
    
    // book_model = Model
	// books = ex_users (databas)
	// book_view = show_data
    // book_id = user_id

    var $table = 'ex_users';

    public function login($username, $password){
        $password = ($password);
            $this->db->where('username',$username);
            $this->db->where('password',$password);
            $query = $this->db->get('ex_users');
            if($query->num_rows()==1){
                foreach ($query->result() as $row){
                    $data = array(
                                'username'=> $row->username,
                                'logged_in'=>TRUE
                            );
                }
                $this->session->set_userdata($data);
                return TRUE;
            }
            else{
                return FALSE;
          }    
        }
        
        public function isLoggedIn(){
                header("cache-Control: no-store, no-cache, must-revalidate");
                header("cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                $is_logged_in = $this->session->userdata('logged_in');
    
                if(!isset($is_logged_in) || $is_logged_in!==TRUE)
                {
                    redirect('/');
                    exit;
                }
        }

    public function get_all()
    {
        $this->db->from('ex_users');
        $query=$this->db->get();
        return $query->result();
    }

    public function get_id($id)
    {
		$this->db->from('ex_users');
		$this->db->where('user_id',$id);
		$query = $this->db->get();

		return $query->row();
    }
    
    public function add_user($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
    
    public function user_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
    }
    
    public function delete_id($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete($this->table);
	}
    
}
?>
