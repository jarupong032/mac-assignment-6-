


<div class="container">

    <h1>Assignment6</h1>

    

<div>
    <button class="btn btn-success" onclick="add_user()">
        <i class="glyphicon glyphicon-plus"></i> 
    </button>
    <button class="btn btn-danger">
    <a href="<?php echo base_url();?>index.php/user/logout"><i class="glyphicon glyphicon-log-out"></i></a>
    </button>
 </div>   
 <br>

    
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
		    <th style="width:100px;">ID</th>
			<th style="width:150px;";>Firstname</th>
			<th style="width:150px;">Lastname</th>
			<th style="width:150px;">Email</th>
			<th style="width:150px;">Phone</th>
            <th style="width:150px;">Action </th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($ex_users as $user){?>
	        <tr>
                <td><?php echo $user->user_id;?></td>
                <td><?php echo $user->user_firstname;?></td>
                <td><?php echo $user->user_lastname;?></td>
                <td><?php echo $user->user_email;?></td>
                <td><?php echo $user->user_phone;?></td>
                <td>
                    <button class="btn btn-warning" onclick="edit_user(<?php echo $user->user_id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
                    <button class="btn btn-danger" onclick="delete_user(<?php echo $user->user_id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                </td>
		    </tr>
		    <?php }?>
        </tbody>
    </table>

</div>
 

<script src="<?php echo base_url('assests/jquery/jquery-3.1.0.min.js')?>"></script>
<script src="<?php echo base_url('assests/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assests/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assests/datatables/js/dataTables.bootstrap.js')?>"></script>


<script type="text/javascript">
$(document).ready( function () {
      $('#table_id').DataTable();
} );

    var save_method;
    var table;
 
    function add_user()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('#modal_form').modal('show');
    }
 
    function edit_user(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
 
        $.ajax({
            url : "<?php echo site_url('user/edit_user/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) 
            {
                $('[name="user_id"]').val(data.user_id);
                $('[name="password"]').val(data.password);
                $('[name="username"]').val(data.username);
                $('[name="user_firstname"]').val(data.user_firstname);
                $('[name="user_lastname"]').val(data.user_lastname);
                $('[name="user_email"]').val(data.user_email);
                $('[name="user_phone"]').val(data.user_phone);
                $('#modal_form').modal('show'); 
                $('.modal-title').text('Edit User'); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
 
    function save()
    {
        var url;
        if(save_method == 'add'){
            url = "<?php echo site_url('user/add_user')?>";
        } else{
            url = "<?php echo site_url('user/user_update')?>";
        }

       // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
                $('#modal_form').modal('hide');
                location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
 
    function delete_user(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
        // ajax delete data from database
            $.ajax({
                url : "<?php echo site_url('user/delete_user')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                { 
                location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
        }
    }



  </script>
 


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
<div class="modal-dialog">
<div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Book Form</h3>
    </div>

    <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
        <input type="hidden" value="" name="user_id"/>
          
        <div class="form-body">

            <div class="form-group">
                <label class="control-label col-md-3">Username</label>
                <div class="col-md-9">
                    <input name="username" placeholder="Username" class="form-control" type="text" >
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Password</label>
                <div class="col-md-9">
                    <input name="password" placeholder="Password" class="form-control" type="password" >
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Firstname</label>
                <div class="col-md-9">
                    <input name="user_firstname" placeholder="Firstname" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Lastname</label>
                <div class="col-md-9">
                    <input name="user_lastname" placeholder="Lastname" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Email</label>
                <div class="col-md-9">
                    <input name="user_email" placeholder="Email" class="form-control" type="text">
                </div>
            </div>
			
            <div class="form-group">
                <label class="control-label col-md-3">Phone</label>
                <div class="col-md-9">
                    <input name="user_phone" placeholder="Phone" class="form-control" type="text">
                </div>
		    </div>

        </div>
        </form>
        </div>

        <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  